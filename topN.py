#!/usr/local/bin/python2

import argparse
import logging
import os
from time import time

debug = False

def topN(filepath, n):
    sorted_list = [None] * n # initialize empty list of size n
    with open(filepath) as fileconents:
        for line in fileconents:
            _append_largest_n_numbers(sorted_list, int(line))
    return sorted_list

def _append_largest_n_numbers(current_highests, new_number):
    '''
    Checks to see if this new number is greater then
    the smallest number we have saved so far.
    '''
    logger = logging.getLogger(__name__)
    logger.debug(current_highests)
    if current_highests[-1] < new_number:
        current_highests[-1] = new_number
        current_highests.sort(reverse=True) # sorts in descending order, O(nlogn) time

def configure_cli():
    parser = argparse.ArgumentParser(description='top N integers finder')
    parser.add_argument('-n','--n', type=int, help='how many of the top numbers to return', required=True)
    parser.add_argument('-f','--filepath', type=str, help='path to file containing numbers to search through', required=True)
    return vars(parser.parse_args())

def configure_logging():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(message)s")
    # Setup console logging
    ch = logging.StreamHandler()
    ch.setLevel(logger.level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

if __name__ == '__main__':
    configure_logging()
    logger = logging.getLogger(__name__)
    user_args = configure_cli()
    
    if not user_args['n'] > 0:
        logger.exception("'{}' is invalid, n must be a positive integer".format(user_args['n']))
    elif not os.path.exists(user_args['filepath']):
        logger.exception("The file '%s' does not exist!", user_args['filepath'])
    else:
        logger.info(topN(user_args['filepath'], user_args['n']))
